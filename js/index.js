$(function(){
  //Manejamos el carousel
  $('.carousel').carousel({
    interval:1000
  })

  //Manejamos el modal
  var newsletterModalVar = document.getElementById('newsletterModal')
  newsletterModalVar.addEventListener('show.bs.modal', function (event) {
    console.log('El Modal se esta abriendo');
    $('#btn-contactinfo').removeClass('btn-secondary');
    $('#btn-contactinfo').addClass('btn-outline-secondary');
    $('#btn-contactinfo').prop('disabled',true);
  });

  newsletterModalVar.addEventListener('shown.bs.modal', function (event) {
    console.log('El Modal esta abierto')
  });

  newsletterModalVar.addEventListener('hide.bs.modal', function (event) {
    console.log('El Modal se esta cerrando')
  });

  newsletterModalVar.addEventListener('hidden.bs.modal', function (event) {
    console.log('El Modal esta cerrado')
    $('#btn-contactinfo').removeClass('btn-outline-secondary');
    $('#btn-contactinfo').addClass('btn-secondary');
    $('#btn-contactinfo').prop('disabled',false);
  });

  //Manejo de los popover
  $('[data-bs-toggle="tooltip"]').tooltip();
  $('[data-bs-toggle="popover"]').popover();

});
